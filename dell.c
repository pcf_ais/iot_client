#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>  /* Объявления стандартных функций UNIX */
#include <fcntl.h>   /* Объявления управления файлами */
#include <errno.h>   /* Объявления кодов ошибок */
#include <termios.h> /* Объявления управления POSIX-терминалом */
#include <sys/types.h>
#include <sys/stat.h>
#include "json/json.h"
int set_interface_attribs(int fd, int speed)
{
    struct termios tty;

    if (tcgetattr(fd, &tty) < 0) {
        printf("Error from tcgetattr: %s\n", strerror(errno));
        return -1;
    }

    cfsetospeed(&tty, (speed_t)speed);
    cfsetispeed(&tty, (speed_t)speed);

    tty.c_cflag |= (CLOCAL | CREAD);    /* ignore modem controls */
    tty.c_cflag &= ~CSIZE;
    tty.c_cflag |= CS8;         /* 8-bit characters */
    tty.c_cflag &= ~PARENB;     /* no parity bit */
    tty.c_cflag &= ~CSTOPB;     /* only need 1 stop bit */
    tty.c_cflag &= ~CRTSCTS;    /* no hardware flowcontrol */

    /* setup for non-canonical mode */
    tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
    tty.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
    tty.c_oflag &= ~OPOST;

    /* fetch bytes as they become available */
    tty.c_cc[VMIN] = 1;
    tty.c_cc[VTIME] = 1;

    if (tcsetattr(fd, TCSANOW, &tty) != 0) {
        printf("Error from tcsetattr: %s\n", strerror(errno));
        return -1;
    }
    return 0;
}

void set_mincount(int fd, int mcount)
{
    struct termios tty;

    if (tcgetattr(fd, &tty) < 0) {
        printf("Error tcgetattr: %s\n", strerror(errno));
        return;
    }

    tty.c_cc[VMIN] = mcount ? 1 : 0;
    tty.c_cc[VTIME] = 5;        /* half second timer */

    if (tcsetattr(fd, TCSANOW, &tty) < 0)
        printf("Error tcsetattr: %s\n", strerror(errno));
}
int main(void)
{
	int data=0;
	char url[40];
	char urlP[40];
	char name[60];
	char nameP[60];
	char httpP[100];
	char key[60];
	char fop[60];
	char port[10];
	char fileRead[500];
	char str[150];
	int lenghtValues=0;
	char eq[60];
	struct json_object *fileReadj;
	struct json_object *buffer;
	FILE *fp;
	printf("Input server address\n");
	scanf("%s",urlP);
	printf("Input name of thing\n");
	scanf("%s",nameP);
	sprintf(fop,"getAndRegister?name=%s",nameP);
	sprintf(httpP,"wget %sgetAndRegister?name=%s",urlP,nameP);
//wget http://springclient.apps.testpcf.org/test/getAndRegister?name=my-dell-gw-1
	fp=fopen(fop, "r+");
	if(fp == NULL)
{
	system(httpP);
	fp=fopen(fop, "r+");
}
	
	fscanf(fp, "%s",fileRead);

	fileReadj=json_tokener_parse(fileRead);//fill struct with data
	//printf("%s",json_object_get_string(fileReadj));
	buffer=json_object_object_get(fileReadj, "some_info");//get internal json from some_info
	//printf("%s",json_object_get_string(buffer));
	buffer=json_object_object_get(buffer, "url");//get url data to struct
	//printf("%s",json_object_get_string(buffer));
	strcpy(url,json_object_get_string(buffer));//copy url data from struct
	printf("url=%s\n",url);

	buffer=json_object_object_get(fileReadj, "some_info");//get internal json from some_info
	//printf("%s",json_object_get_string(buffer));
	buffer=json_object_object_get(buffer, "appId");//get name data to struct
	//printf("%s",json_object_get_string(buffer));
	strcpy(name,json_object_get_string(buffer));//copy name data from struct
	printf("name=%s\n",name);
	
	buffer=json_object_object_get(fileReadj, "some_info");
	buffer=json_object_object_get(buffer, "lengthValues");//get internal json from some_info
	lenghtValues=json_object_get_int(buffer);//copy url data from struct
	printf("things=%d\n",lenghtValues);

	for(int i=0;i<lenghtValues;i++)
{

	buffer=json_object_object_get(fileReadj, "some_info");//get internal json from some_info
	//printf("%s",json_object_get_string(buffer));
	buffer=json_object_object_get(buffer, "values");//get values data to struct
	//printf("%s",json_object_get_string(buffer));
	buffer=json_object_array_get_idx(buffer,i);//parse massive
	buffer=json_object_object_get(buffer, "name");
	strcpy(eq,json_object_get_string(buffer));
	printf("eq=%s\n",eq);
	if(strcmp (nameP, eq)==0)
	{
	buffer=json_object_object_get(fileReadj, "some_info");//get internal json from some_info
	//printf("%s",json_object_get_string(buffer));
	buffer=json_object_object_get(buffer, "values");//get values data to struct
	//printf("%s",json_object_get_string(buffer));
	buffer=json_object_array_get_idx(buffer,i);//parse massive
	buffer=json_object_object_get(buffer, "key");//get key data to struct
	strcpy(key,json_object_get_string(buffer));//copy name data from struct
	printf("key=%s\n",key);
	//break;
	}

}
	buffer=json_object_object_get(fileReadj, "some_info");//get internal json from some_info
	//printf("%s",json_object_get_string(buffer));
	buffer=json_object_object_get(buffer, "numPort");//get port data to struct
	//printf("%s",json_object_get_string(buffer));
	strcpy(port,json_object_get_string(buffer));//copy port data from struct
	printf("port=%s\n",port);	

	
	//while(1)
	//{
	//data=read_uart();
	//sprintf(str,"zabbix_sender -z %s -p %s -s \"%s\" -k %s -o %d",url,port,name,key,data);
	//system(str);
	//printf("%s",str);
	//printf("%d",data);
	//sleep(1);
	//}
	

	char *portname = "/dev/ttyUSB1";
    	int fd;
   	 int wlen;

    fd = open(portname, O_RDWR | O_NOCTTY | O_SYNC);
    if (fd < 0) {
        printf("Error opening %s: %s\n", portname, strerror(errno));
        return -1;
    }
    set_interface_attribs(fd, B9600);
      

         char buf[80];
        int rdlen;
do {
        unsigned char buf[80];
        int rdlen;
	wlen = write(fd, "@*1\r", 4);
    
    tcdrain(fd);  
        rdlen = read(fd, buf, sizeof(buf) - 1);
        if (rdlen > 0) {

            unsigned char   *p;
            //printf("Read %d:", rdlen);
            for (p = buf; rdlen-- > 0; p++)
{
                //printf(" 0x%x", *p);
		sprintf(buf,"0x%x\n", *p);
           //printf("\n");
		int number = (int)strtol(buf, NULL, 0);
	//printf("%d\n",number);
if(number!= 64&&number!=120)
{
		sprintf(str,"zabbix_sender -z %s -p %s -s \"%s\" -k %s -o %d",url,port,name,key,number);
		system(str);
		printf("%s\n",str);
		 
}

}
        } else if (rdlen < 0) {
            printf("Error from read: %d: %s\n", rdlen, strerror(errno));
        }
        
    } while (1);

}
